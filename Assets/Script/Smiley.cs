﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smiley : MonoBehaviour
{
    //######################################################################
    //##                      ATTRIBUTS PUBLIQUES
    //######################################################################

    public GameObject coin;
    public GameObject canvas;
    public GameObject manager;


    //######################################################################
    //##                      ATTRIBUTS PRIVES
    //######################################################################

    private bool pushed = false;


    // active l'animation du smiley enfoncé (enlève l'ombre + rescale)
    public void OnPointerDown ()
    {
        pushed = true;

        GetComponent<AnimatorQM>().SmileyPushed();

        Vector2 spawnPosition = Camera.main.ViewportToWorldPoint(new Vector2(Random.value, Random.value));

        GameObject newCoin = Instantiate(coin, spawnPosition, Quaternion.identity, canvas.transform);

        // donne à manger le Manager à chaque newCoin (cf la fonction dans Coin)
        newCoin.GetComponent<Coin>().Initiliaze(manager.GetComponent<Manager>());

        // ajoute newCoin à la liste CoinList du manager
        manager.GetComponent<Manager>().AddCoinToList(newCoin);
    }

    // désactive l'animation du smiley enfoncé en passant par l'anim flash
    public void OnPointerUp ()
    {
        // utilise un if avec un flag sinon le trigger reste en mémoir quand on release le clic hors du smiley
        if(pushed == true)
        {
            GetComponent<AnimatorQM>().SmileyClicked();
        }

        GetComponent<AnimatorQM>().SmileyReleased();
    }

    // désactive l'animation du smiley enfoncé
    public void OnPointerExit ()
    {
        pushed = false;

        GetComponent<AnimatorQM>().SmileyReleased();
    }
}
