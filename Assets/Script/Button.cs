﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    //######################################################################
    //##                      ATTRIBUTS PUBLIQUES
    //######################################################################

    public bool isJetonEnable = false;
    public bool isButtonClicked = false;
    public bool isClicButtonIsCall = false;
    public Vector3 posJetonInitial;
    public GameObject jeton;
    public GameObject manager;
    public GameObject zoneA;


    // Start is called before the first frame update
    void Start()
    {
        // récupère la position de jeton au début
        posJetonInitial = jeton.GetComponent<RectTransform>().position;
    }

    public void ClicJetonButton()
    {
        // flag pour destroy tout les coins en cliquant sur le bouton
        isClicButtonIsCall = true;

        if (isJetonEnable == false)
        {
            // active le jeton
            jeton.gameObject.SetActive(true);

            // lui donne sa position initiale
            jeton.transform.position = posJetonInitial;

            // Toggle le flag 
            isJetonEnable = true;

            isButtonClicked = false;

        }
        else
        {
            // reinitialise la coroutine via la fonction stopTimer()
            jeton.GetComponent<Jeton2>().StopTimer();

            if (jeton.GetComponent<Jeton2>().isMoving)
            {
                isButtonClicked = true;
            }
            else
            {
                // lance l'animation fadeout dans laquelle se trouve l'Event Animator qui deasable jeton
                jeton.GetComponent<AnimatorQM>().StartFadeOut();

                isButtonClicked = false;

                isJetonEnable = false;
            }
        }
    }
}
