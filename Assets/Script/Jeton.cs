﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jeton : MonoBehaviour
{
    public bool clic = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        // si on clique sur l'objet, il prend la même position que le curseur
        if (clic == true)
        {
            // récupère la position du curseur en fonction de la caméra
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = new Vector3 (mousePos.x, mousePos.y, 0f);
        }
    }

    // définit Clic = true si on appel cette fonction via Event Trigger
    public void OnPointerDown()
    {
        clic = true;
    }

    // définit Clic = false si on appel cette fonction via Event Trigger
    public void OnPointerUp()
    {
        clic = false;
    }
}
