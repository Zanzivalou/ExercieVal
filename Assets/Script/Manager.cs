﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    //######################################################################
    //##                      ATTRIBUTS PUBLIQUES
    //######################################################################

    public bool pointerIsDown;
    public float speed;
    public GameObject smiley;
    public GameObject jeton;
    public GameObject button;
    public GameObject zoneA;
    public GameObject coin;
    public List<GameObject> coinList = new List<GameObject>();

    //######################################################################
    //##                      ATTRIBUTS PRIVEES
    //######################################################################

    public Vector3 mousePosition;


    void Update()
    {
        // si le bouton est cliqué
        if(button.GetComponent<Button>().isClicButtonIsCall)
        {
            StartCoroutine("DelayDestroyCoin");

            // re set le flag à false
            button.GetComponent<Button>().isClicButtonIsCall = false;
        }

        if (button.GetComponent<Button>().isJetonEnable)
        {
            // Si le flag est activer
            if (jeton.GetComponent<Jeton2>().isMoving == true )
            {
                
                QuickMove();

                if (jeton.transform.position == mousePosition)
                {
                    jeton.GetComponent<Jeton2>().isMoving = false;

                    // appel la fonction de l'AnimatorQMqui arrète l'anim QuickMove
                    jeton.GetComponent<AnimatorQM>().StopMoving();

                    // appel la fonction checkzone de la classe Jeton2
                    jeton.GetComponent<Jeton2>().CheckZone(zoneA);

                    if (button.GetComponent<Button>().isButtonClicked)
                    {
                        // lance l'animation fadeout dans laquelle se trouve l'Event Animator qui deasable jeton
                        jeton.GetComponent<AnimatorQM>().StartFadeOut();

                        button.GetComponent<Button>().isButtonClicked = false;

                        button.GetComponent<Button>().isJetonEnable = false;
                    }
                }
            }
        }
    }

    public void GetMousePosition()
    {
        // récupère la position du curseur en fonction de la caméra
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // garder le z de la souris à 0
        mousePosition.z = jeton.transform.position.z;
    }

    public void QuickMove()
    {
        float step = speed * Time.deltaTime;

        // fonction duMoveTowards. envois l'objet à bouger vers une target, avec une certaine vitesse step
        jeton.transform.position = Vector3.MoveTowards(jeton.transform.position, mousePosition, step);
    }

    // fonction appelé via l'event trigger du "net" qui valide si on clic dessus. pour ne pas que ca foutte un problème quand
    // on clic sur le bouton. Celui ci dépend de sa place dans la hierarchie
    public void ClicScreen()
    {
        if (button.GetComponent<Button>().isButtonClicked == false)
        {
            // reinitialise la coroutine via la fonction stopTimer()
            jeton.GetComponent<Jeton2>().StopTimer();

            GetMousePosition();

            // flag
            jeton.GetComponent<Jeton2>().isMoving = true;

            if (button.GetComponent<Button>().isJetonEnable)
            {

                // appel la fonction de l'AnimatorQMqui déclanche l'anim QuickMove
                jeton.GetComponent<AnimatorQM>().StartMoving();
            }
        }
    }

    // Fonction appelé du boutton pour ajouter chaque coin à la liste
    public void AddCoinToList (GameObject newCoin)
    {
        coinList.Add(newCoin);
    }

    // fonction appelé de Coin qui supprime supprime les newcoin de la liste
    public void SubCoinFromList(GameObject coinToSub)
    {
        coinList.Remove(coinToSub);
    }

    // coroutine qui fait detruire les coin avec 0.5 sec d'intervalle
    IEnumerator DelayDestroyCoin()
    {
        // creeer une liste temporaire pour ne pas modifier la liste de base dans le foreach (sinon ca marche pas)
        List<GameObject> tempList = new List<GameObject>();
        tempList.AddRange(coinList);

        // pour chaque newcoin dans la liste
        foreach (GameObject newCoin in tempList)
        {
            yield return new WaitForSeconds(0.5f);

            newCoin.GetComponent<Coin>().DestroyCoinAnimation();

        }

        // supprime toute les éléments de la liste dans le doute
        coinList.Clear();
    }
}
