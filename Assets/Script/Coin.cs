﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    //######################################################################
    //##                      ATTRIBUTS PUBLIQUES
    //######################################################################

    private Manager manager;
    
    // à chaque fois qu'on instancie un newCoin, il recupère le manager via cette fonction qui est appelé de Smiley
    public void Initiliaze(Manager manager)
    {
        this.manager = manager;
    }

    // appel la fonction de l'animator qui détruit la coin avec un event animation à la fin
    public void DestroyCoinAnimation()
    {
        GetComponent<AnimatorQM>().CoinDestroy();

        // Lance la fonction du manager qui supprime un element de la liste
        manager.SubCoinFromList(this.gameObject);
    }

    // event fonction appelé à la fin de l'anim CoindDestroy
    public void DestroyCoin()
    {
        Destroy(gameObject);
    }

}
