﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jeton2 : MonoBehaviour
{

    //######################################################################
    //##                      ATTRIBUTS PUBLIQUES
    //######################################################################

    public bool pointerIsDown = false;
    public bool isInZoneA = false;
    public bool isMoving = false;
    public Vector3 newPositionDragAndDrop;
    public GameObject zoneA;
    public GameObject smiley;
    public GameObject manager;
    public GameObject textTimer;
    public GameObject texteSmiley;
    public GameObject button;
    public Coroutine cr;

    //######################################################################
    //##                      ATTRIBUTS PRIVEES
    //######################################################################


    // Update is called once per frame
    void Update()
    {

            // si on clique sur l'objet, il prend la même position que le curseur
            if (pointerIsDown == true)
            {
                // récupère la position du curseur en fonction de la caméra
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                transform.position = new Vector3(mousePos.x, mousePos.y, transform.position.z);
            }

    }

    // définit Clic = true si on appel cette fonction via Event Trigger
    public void OnPointerDown()
    {
        // si le boutton n'est pas activé, il réalise la fonction. Sinon il ne fait rien (dans le cas ou on clic sur le boutton pendant un quick move)
        if (button.GetComponent<Button>().isButtonClicked == false)
        {
            // reinitialise la coroutine via la fonction stopTimer()
            StopTimer();

            pointerIsDown = true;

            GetComponent<AnimatorQM>().StartDraging();

            // si le jeton est en quick move, il ne continu plus sa course et arrete ses animations
            if (isMoving == true)
            {
                GetComponent<AnimatorQM>().StopMoving();

                GetComponent<Jeton2>().CheckZone(zoneA);

                isMoving = false;
            }
        }
    }

    // définit Clic = false si on appel cette fonction via Event Trigger
    public void OnPointerUp()
    {
        pointerIsDown = false;
        GetComponent<AnimatorQM>().StopDraging();

        CheckZone(zoneA);
    }

    // fonction appelé dans l'anim FadeOut avec un animator Event
    public void Disable()
    {
        gameObject.SetActive(false);
    }

    // check si le jeton se trouve dans la zone A
    public void CheckZone(GameObject zone)
    {
        newPositionDragAndDrop = GetComponent<Transform>().position;

        // utilise le GetWorldCorners pour calculer la taille de la zone à partir d'un array
        Vector3[] v = new Vector3[4];
        zoneA.GetComponent<RectTransform>().GetWorldCorners(v);

        Rect rect = Rect.MinMaxRect(v[0].x, v[0].y, v[2].x, v[2].y);

        // utilise la fonction rect.Contains qui check si le game object est dans la zone
        if(rect.Contains(newPositionDragAndDrop))
        {
            smiley.GetComponent<AnimatorQM>().Smile();

            isInZoneA = true;

            cr =  StartCoroutine("TexteSmiley");

        }
        else
        {
            smiley.GetComponent<AnimatorQM>().Sad();

            isInZoneA = false;

            cr = StartCoroutine("TexteSmiley");

        }
    }

    // fonction qui stop la coroutine et qui reinitialise le texte timer
    public void StopTimer ()
    {
        // vérifie si il y a bien une coroutine qui est activé
        if (cr != null)
        {
            StopCoroutine(cr);
            textTimer.SetActive(false);
        }
    }

    // coroutine qui attend timer pour ecrire "Je suis content"
    IEnumerator TexteSmiley()
    {
        //active le texte timer
        textTimer.SetActive(true);

        int timer = 5;

        // fait le décompte du timer
        while (timer > 0)
        {
            textTimer.GetComponent<Text>().text = "TIMER : " + timer.ToString();
            yield return new WaitForSeconds(1);
            timer--;
        }

        textTimer.SetActive(false);

        if(isInZoneA)
        {
            texteSmiley.GetComponent<Text>().text = "JE SUIS CONTENT !!!";
        }
        else
        {
            texteSmiley.GetComponent<Text>().text = "JE SUIS PAS CONTENT !!!!!";
        }
    }
}
