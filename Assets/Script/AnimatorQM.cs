﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorQM : MonoBehaviour
{
    //######################################################################
    //##                      ATTRIBUTS PRIVEE
    //######################################################################

    private Animator animator;


    public void Awake()
    {
        animator = GetComponent<Animator>();
    }


    public void StartMoving()
    {
        animator.SetBool("isMoving", true);
    }

    public void StopMoving()
    {
        animator.SetBool("isMoving", false);
    }

    public void StartDraging()
    {
        animator.SetBool("isDraging", true);
    }

    public void StopDraging()
    {
        animator.SetBool("isDraging", false);
    }

    public void StartFadeOut()
    {
        // l'utilisation d'un trigger est plus judicieu dans ce cas (trigger = envois un signal bref)
        animator.SetTrigger("FadeOut");
    }

    public void Smile ()
    {
        animator.SetBool("isInZoneA", false);
    }

    public void Sad()
    {
        animator.SetBool("isInZoneA", true);
    }

    public void SmileNoShadow()
    {
        animator.SetBool("isPointerDown", true);

    }

    public void SadNoShadow()
    {
        animator.SetBool("isPointerDown", false);
    }

    public void SetIsPointerExitFalse ()
    {
        animator.SetBool("isPointerExit", false);
    }

    public void NoFlash()
    {
        animator.SetBool("isPointerExit", true);
    }

    public void SmileyPushed ()
    {
        animator.SetBool("push", true);
    }

    public void SmileyReleased ()
    {
        animator.SetBool("push", false);
    }

    public void SmileyClicked()
    {
        animator.SetTrigger("click");
    }

    public void CoinDestroy()
    {
        animator.SetTrigger("destroy");
        Debug.Log("destroy anim");
    }
}
